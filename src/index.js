import { Component, html } from "@exalt/core";
import "./index.css";
import Prism from 'prismjs';

export class App extends Component {

	state = {

		api_key: "",
		auth_msg: "",

		access_token: "",
		collections: [],
		url: "",
		type: "link",
		title: "",
		description: "",
		thumbnail: "",
		tags: "",
		parent: "",
		token_msg: ""

	}

	async highlight() {
		Prism.highlightAll();
	}

	async mount() {
		this.highlight();
	}

	async getToken() {
		// Get access token based on the linkish API key
		fetch("https://api.linkish.io/get-token", {
			method  : "POST", 
			body : JSON.stringify({
				"api_key": this.state.api_key
			}),
			headers: {
				"Content-Type": "application/json",
				"Accept": "application/json",
			}
		})
		.then(response => response.json())
		.then((response) => {
			if(response.success){
				this.state.access_token = response.token;
				this.state.auth_msg = response.message;

				// Get collections based on the access token
				fetch("https://api.linkish.io/get-collections", {
					method  : "GET",
					headers: {
						"Authorization": `Bearer ${this.state.access_token}`,
					}
				})
				.then(response => response.json())
				.then((response) => {
					this.state.collections = response;
					this.highlight();
				}, (error) => {
					console.log(error);
				});

			}
		}, (error) => {
			console.log(error);
		});
	}

	async saveLink() {
		// Save link in your linkish dashboard
		fetch("https://api.linkish.io/save-link", {
			method  : "POST", 
			body : JSON.stringify({
				"url": this.state.url,
				"type": this.state.type,
				"desc": this.state.desc,
				"title": this.state.title,
				"thumbnail": this.state.thumbnail,
				"parent": this.state.parent,
				"tags": [this.state.tags], // takes only array
			}),
			headers: {
				"Authorization": `Bearer ${this.state.access_token}`,
				"Content-Type": "application/json",
				"Accept": "application/json",
			}
		})
		.then(response => response.json())
		.then((response) => {
			this.state.token_msg = response.message;
			this.highlight();
		}, (error) => {
			console.log(error);
		});
	}

	onUpdate(key, value) {
		this.highlight();
	}

	render() {
		return html`
			<div class="min-h-screen bg-gray-100 px-10 py-10 flex flex-col justify-center">
				<div class="relative pb-5 mb-20 border-b-4">
					<div class="text-5xl font-bold text-gray-900 text-center">
						<h2>API demo for linkish.io</h2>
						<p class="text-sm text-gray-500 font-normal pt-5 pb-10"><a class="underline" href="https://auth.linkish.io/register?from=linkish" target="_blank">Sign up</a> to create an API key. Read full documentation at <a class="underline" href="https://help.linkish.io/api-docs" target="_blank">our knowledgebase.</a></p>
					</div>
				</div>
				<div class="relative md:flex rounded-3xl mb-5">
					<div class="text-2xl font-bold leading-7 text-gray-900 sm:text-3xl sm:truncate flex items-center space-x-5">
						<div class="h-14 w-14 bg-yellow-200 rounded-full flex flex-shrink-0 justify-center items-center text-yellow-500 text-2xl font-mono">#1</div>
						<div>
							<h2>Generate access token</h2>
							<p class="text-sm text-gray-500 font-normal leading-relaxed">Create an API key from <a href="https://linkish.io/settings" target="_blank" class="underline">settings page</a> and paste it below to generate access token</p>
						</div>
					</div>
				</div>
				<div class="relative md:flex step-block rounded-3xl mb-20 shadow">
					<div class="relative bg-white rounded-3xl p-10 flex-none w-full md:w-1/4">
						<div class="max-w-md mx-auto">
							<div class="divide-gray-200">
								<div class="pb-2 text-base leading-6 space-y-4 text-gray-700 sm:text-lg sm:leading-7">
									<div class="flex flex-col">
										<label class="leading-loose">API key</label>
										<input type="text" class="px-4 py-2 border focus:ring-gray-500 focus:border-gray-900 w-full sm:text-sm border-gray-300 rounded-md focus:outline-none text-gray-600" placeholder="Enter your unique API key" value="${this.state.api_key}" onchange="${(e) => this.state.api_key = e.target.value}">
									</div>
								</div>
								<div class="pt-4 flex items-center space-x-4">
									<button class="bg-blue-500 flex justify-center items-center w-full text-white px-4 py-3 rounded-md focus:outline-none" onclick=${() => this.getToken()}>Generate token</button>
								</div>
							</div>
							${this.state.auth_msg != "" ?
								`<div class="text-white px-6 py-4 border-0 rounded-lg relative mt-8 bg-green-500">
									<span class="inline-block align-middle mr-8">
										<b class="capitalize">${this.state.auth_msg}</b>
									</span>
								</div>` : ``
							}
						</div>
					</div>
					<div class="w-full md:w-3/4 text-white px-5">
						<pre><code class="language-javascript">
// Get access token based on the linkish API key
fetch("https://api.linkish.io/get-token", {
	method : "POST", 
	body : JSON.stringify({
		"api_key": "${this.state.api_key || 'API_KEY'}"
	}),
	headers: {
		"Content-Type": "application/json",
		"Accept": "application/json",
	}
}).then(response => response.json()).then((response) => {
	// Save the token generated from the API key to use in other API
});
						</code></pre>
					</div>
				</div>


				<div class="relative md:flex rounded-3xl mb-5">
					<div class="text-2xl font-bold leading-7 text-gray-900 sm:text-3xl sm:truncate flex items-center space-x-5">
						<div class="h-14 w-14 bg-yellow-200 rounded-full flex flex-shrink-0 justify-center items-center text-yellow-500 text-2xl font-mono">#2</div>
						<div>
							<h2>Create new link<h2>
							<p class="text-sm text-gray-500 font-normal leading-relaxed">Fill in link details with prefilled access token</p>
						</div>
					</div>
				</div>
				<div class="relative md:flex step-block rounded-3xl shadow">
					<div class="relative bg-white rounded-3xl p-10 flex-none w-full md:w-1/4">
						<div class="max-w-md mx-auto">
							<div class="divide-gray-200">
								<div class="pb-2 text-base leading-6 space-y-4 text-gray-700 sm:text-lg sm:leading-7">
									<div class="flex flex-col">
										<label class="leading-loose">Access token <small class="text-gray-400">(auto-generated from above)</small></label>
										<input type="text" class="px-4 py-2 border focus:ring-gray-500 focus:border-gray-900 w-full sm:text-sm border-gray-300 rounded-md focus:outline-none text-gray-600" placeholder="Generate a token in Step 1" value="${this.state.access_token}" onchange="${(e) => this.state.access_token = e.target.value}" disabled readonly>
									</div>
									<div class="flex flex-col">
										<label class="leading-loose">Collection <small class="text-gray-400">(auto-populated based on token above)</small></label>
										<select class="px-4 py-2 border focus:ring-gray-500 focus:border-gray-900 w-full sm:text-sm border-gray-300 rounded-md focus:outline-none text-gray-600" value="${this.state.parent}" onchange="${(e) => this.state.parent = e.target.value}">
											${this.state.collections.map(collection => `<option value="${collection.id}">${collection.name}</option>`)}
										</select>
									</div>
									<div class="flex flex-col">
										<label class="leading-loose">Type <small class="text-gray-400">(required)</small></label>
										<select class="px-4 py-2 border focus:ring-gray-500 focus:border-gray-900 w-full sm:text-sm border-gray-300 rounded-md focus:outline-none text-gray-600" value="${this.state.type}" onchange="${(e) => this.state.type = e.target.value}">
											<option value="link">Link</option>
											<option value="text">Text</option>
										</select>
									</div>
									<div class="flex flex-col">
										<label class="leading-loose">Link <small class="text-gray-400">(required if Type above is "link")</small></label>
										<input type="text" class="px-4 py-2 border focus:ring-gray-500 focus:border-gray-900 w-full sm:text-sm border-gray-300 rounded-md focus:outline-none text-gray-600" placeholder="https://example.com/" value="${this.state.url}" onchange="${(e) => this.state.url = e.target.value}">
									</div>
									<div class="flex flex-col">
										<label class="leading-loose">Title</label>
										<input type="text" class="px-4 py-2 border focus:ring-gray-500 focus:border-gray-900 w-full sm:text-sm border-gray-300 rounded-md focus:outline-none text-gray-600" placeholder="Custom title" value="${this.state.title}" onchange="${(e) => this.state.title = e.target.value}">
									</div>
									<div class="flex flex-col">
										<label class="leading-loose">Description</label>
										<input type="text" class="px-4 py-2 border focus:ring-gray-500 focus:border-gray-900 w-full sm:text-sm border-gray-300 rounded-md focus:outline-none text-gray-600" placeholder="Custom description" value="${this.state.desc}" onchange="${(e) => this.state.desc = e.target.value}">
									</div>
									<div class="flex flex-col">
										<label class="leading-loose">Thumbnail URL</label>
										<input type="text" class="px-4 py-2 border focus:ring-gray-500 focus:border-gray-900 w-full sm:text-sm border-gray-300 rounded-md focus:outline-none text-gray-600" placeholder="https://example.com/image.png" value="${this.state.thumbnail}" onchange="${(e) => this.state.thumbnail = e.target.value}">
									</div>
									<div class="flex flex-col">
										<label class="leading-loose">Tags</label>
										<input type="text" class="px-4 py-2 border focus:ring-gray-500 focus:border-gray-900 w-full sm:text-sm border-gray-300 rounded-md focus:outline-none text-gray-600" placeholder="tag1, tag2" value="${this.state.tags}" onchange="${(e) => this.state.tags = e.target.value}">
									</div>
								</div>
								<div class="pt-4 flex items-center space-x-4">
									<button class="bg-blue-500 flex justify-center items-center w-full text-white px-4 py-3 rounded-md focus:outline-none" onclick=${() => this.saveLink()}>Create link</button>
								</div>
							</div>
							${this.state.token_msg != "" ?
								`<div class="text-white px-6 py-4 border-0 rounded-lg relative mt-8 bg-green-500">
									<span class="inline-block align-middle mr-8">
										<b class="capitalize">${this.state.token_msg}</b>
									</span>
								</div>` : ``
							}
						</div>
					</div>
					<div class="w-full md:w-3/4 text-white px-5">
						<pre><code class="language-javascript">
// Get collections based on the access token
fetch("https://api.linkish.io/get-collections", {
	method : "GET",
	headers: {
		"Authorization": "Bearer ${this.state.access_token ? this.state.access_token.substring(0, 20)+'...' : 'ACCESS_TOKEN'}",
	}
}).then(response => response.json()).then((response) => {
	// Use the list to choose the collection where the link will be saved
});



// Save link in your linkish dashboard
fetch("https://api.linkish.io/save-link", {
	method : "POST", 
	body : JSON.stringify({
		"type": "string", // either "link" or "text"
		"url": "string", // required if "type" is "link"
		"desc": "string", // card description, if empty it will be scraped from "url"
		"title": "string", // card title, if empty it will be scraped from "url"
		"thumbnail": "string", // preview image URL, if empty it will be scraped from "url"
		"parent": "string", // collection.id recieved from get-collection API
		"tags": ["string"], // an array of strings, items can also have comma seprated strings
	}),
	headers: {
		"Authorization": "Bearer ${this.state.access_token ? this.state.access_token.substring(0, 20)+'...' : 'ACCESS_TOKEN'}",
		"Content-Type": "application/json",
		"Accept": "application/json",
	}
}).then(response => response.json()).then((response) => {
	// Do something with response
});
</code></pre>
					</div>
				</div>
			</div>
		`;
	}
}

Component.create({ name: "app-root", useShadow: false }, App);